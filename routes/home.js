const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const router = express.Router();
const passport = require('passport');

function ifAdmin(req) {
    if (!req.user) return; // 'Not authorized'
    else if (req.user.role !== 1) return; // 'Forbidden'
    else return 1;  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

router.get('/', function(req, res) {
    res.render('index', {user:req.user,"admin":ifAdmin(req)});
});

router.get('/about', function(req, res) {
    res.render('about', { user:req.user,"admin":ifAdmin(req) });
});


module.exports = router;