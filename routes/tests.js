const express = require('express');

const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const path = require('path');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');


const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(busboyBodyParser({ limit: '5mb' }));

const extra = require('../models/extra');

router.get('/new', function(req, res) {
    question_c.getAll()
    .then((data)=>{return Promise.all([data,user_c.getAll()])})
    .then(([data,authors])=>{
        const QData = {
            "all_items":    data,
            "all_authors":  authors,
            "user":         req.user,
            "admin":        extra.checkAdmin(req)};
        res.render('test_creation',QData);
    })
    .catch((err)=> res.status(500).render("500"))
});

router.post('/update/test_photo/:id', async function(req, res) {
    let test = await test_c.getById(req.params.id);
    if(req.user._id.toString()!=test.authorID.toString() && req.user.role!=1){
        res.status(403).render("403");
    } 
    else{res.render("picture_adding",{
            "tempId": req.params.id,
            "user":         req.user,
            "admin":        extra.checkAdmin(req)});}
});

router.post('/new',function(req, res) {
            
        let authorID = req.user._id; 
        console.log(authorID);
            let questions = [];
            let numOfQuestions = 0;
            console.log(req.body);
        
            if(req.body.test_questions instanceof Array){
                for(let question of req.body.test_questions){
                    questions[numOfQuestions] = question;
                    numOfQuestions++;
                }
            }else {
                questions[0] = req.body.test_questions;
                numOfQuestions++;
            }
            let temp = new test_c(      req.body.test_name,
                questions,              '../',
                authorID,               req.body.test_subject,  
                req.body.test_rating,   0,                   
                numOfQuestions
            );
            test_c.insert(temp)
                .then((tempp)=>{res.render("picture_adding",{
                    "tempId": tempp.id,
                    "user":         req.user,
                    "admin":        extra.checkAdmin(req)})
                })
                .catch((err)=>res.status(500).render("500"))     
        
});


router.post('/update',async function(req, res) {
    let id = req.body.id; 
    let test = await test_c.getById(id);
    if(req.user._id.toString()!=test.authorID.toString() && req.user.role!=1){
        res.status(403).render("403");
    } 
    else{    let objQuest = {
            "id":                   req.body.id,  
            "name":                 req.body.test_name,
            "rating":               req.body.test_rating,
            "subject":              req.body.test_subject,
        }

        test_c.update(objQuest)
                  .then((id)=>{
                      res.redirect("../tests/" + id);})
                  .catch((err)=>res.status(500).render("500"))
    }     
});
router.get('/',function(req, res,next) {
        /*test_c.getAll()
            .then((data)=>extra.pagination(res,req,'tests',data,5))
            .catch((err)=> res.status(404).send(err.mes))*/
        res.setHeader('Content-Type', 'text/html');
        res.render("tests.mst",{ 
            "user":         req.user,
            "admin":        extra.checkAdmin(req)
        });

});

router.get('/:id',function(req, res) {
    
    test_c.getById(req.params.id)
        .then((choosed_test)=>{
            return Promise.all([choosed_test,user_c.getById(choosed_test.authorID)])})
        .then(([choosed_test,author])=>{
            return Promise.all([choosed_test,author,question_c.getAll()])})
        .then(([choosed_test,author,pack])=>{
                let ret = [];
                for(let question of pack){
                    let i = 0;
                    while(i<question.tests.length){
                        if(question.tests[i].toString()==choosed_test.id.toString()) {
                            ret.push(question);
                            break;
                        }
                        i++;
                    }
                }
                let isAuthor;
                if(req.user._id.toString()==author._id.toString() || req.user.role==1){
                    isAuthor= true;
                }
                const testData = {
        
                    "id":                   choosed_test.id,
                    "name":                 choosed_test.name,
                    "picUrl":               choosed_test.picUrl,
                    "author":               author.login,
                    "authorUrl":            "../users/"+author.id,
                    "created":              choosed_test.created,
                    "rating":               choosed_test.rating,
                    "subject":              choosed_test.subject,
                    "numOfpeopleWhoPassed": choosed_test.numOfpeopleWhoPassed,
                    "numOfQuestions":       choosed_test.numOfQuestions,
                    "all_items":            ret,
                    "user":                 req.user,
                    "isAuthor":             isAuthor,
                    "admin":                extra.checkAdmin(req)             
                };    
                res.setHeader('Content-Type', 'text/html');
                res.render('test', testData);
            }) 
    .catch((err)=>res.status(404).render("404"))
});
  
module.exports = router;