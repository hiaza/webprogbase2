const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const router = express.Router();
const passport = require('passport');
const config = require('../config');
const cloudinary = require('cloudinary');
const extra = require('../models/extra');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});


function checkPasswords(pass1,pass2){
    if(pass1===pass2){
        return true;
    }else return false;
};

function loginIsCorrect(login){
    if(/^[a-zA-Z1-9]+$/.test(login) === false)
        return {status:false,error:"only+latin+letters+and+numbers"};
    if(login.length < 4 || login.length > 20)
        return {status:false,error:"login+length+must+be+more+then+4+and+less+then+20"};
    if(parseInt(login.substr(0, 1)))
        return {status:false,error:"login+has+to+begin+with+letter"};

     return {status:true,error:" "};
}
    
function passIsCorrect(textPass,textPass2){
    if(textPass != textPass2){
        return {status:false,error:"password1+not+equal+password2"};
    }
      var r=/[^A-Z-a-z-0-9]/g; 
      if(r.test(textPass)){
          return {status:false,error:"only+latin+letters+and+numbers"};
      }
      if (textPass.length<6){
          return {status:false,error:"password+is+too+short"};
      }
      if (textPass.length>20){
          return {status:false,error:"password+is+too+long"};
      }return {status:true,error:" "};
}


router.get('/register',function(req, res,next) {
    res.render("registration", { user: req.user,error1:req.query.error1,error2:req.query.error2});
});

router.post('/register',function(req, res,next) {

    let pic = "/images/unavailablePhoto.png";        
    let login = req.body.login;                     
    let temp = new user_c(login.toLowerCase(),
    extra.getHash(req.body.password1).passwordHash,
    0, "Name" + " " + "Surname",
    pic,false,"I'm a new User!!!","");
    user_c.insert(temp)
            .then(()=>{res.redirect("/auth/login")})
            .catch((err)=>res.send(err))
});

router.get('/login',function(req, res,next) {
    if(req.query.message){
        console.log(req.query.message);
        res.render("login",{'error':req.query.message});
    }
    else res.render("login");
});



router.post('/login',
passport.authenticate('local', { failureRedirect: '/auth/login?message=incorrect+login+or+password' }),  
(req, res) => {
    if(req.user.isDisabled==false){
        res.redirect('/');
    }else{
        req.logout();
        res.redirect('/auth/login?message=your+account+is+disabled');
    }
});

router.get('/logout', 
(req, res) => {
    req.logout();   
    res.redirect('/');
});

router.get('/google',
passport.authenticate('google', { scope: 
    ['email'] }
));

router.get( '/google/redirect', 
passport.authenticate( 'google', { 
    successRedirect: '/',
    failureRedirect: '/auth/login?message=your+account+is+disabled+or+deleted'
}));

// router.get( '/google/redirect', 
//   passport.authenticate( 'google', { 
//       successRedirect: '/',
//       failureRedirect: '/auth/login'
// }));
/*
router.post('/login',function(req, res,next) {
    res.end("login page post");
    //res.render("register_page");
});
*/
/*
router.post('/logout',function(req, res,next) {
    res.end("logout page post");
    //res.render("register_page");
});*/
module.exports = router;