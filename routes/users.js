const express = require('express');
const user_c = require('../models/user');

const extra = require('../models/extra');
const router = express.Router();


function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 1) res.status(403).render("403"); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}


router.get('/',checkAdmin,function(req, res,next) {
    console.log(`got some request`);
    user_c.getAll()
        .then((users_all)=>{
        const usersData = {users_all,
            "user":req.user,
            "admin": extra.checkAdmin(req)};
        res.setHeader('Content-Type', 'text/html');    
        res.render('users', usersData);
        })
        .catch((err)=>res.status(403).render("403"))
});

router.get('/:id/changeInfo',function(req, res,next) {
if(req.params.id==req.user._id||req.user.role==1){
    user_c.getById(req.params.id)
        .then((user)=>{
            let dateOfBirth;
            if(user.dateOfBirth!=null){
                dateOfBirth = Date.parse(user.dateOfBirth);
            }
            const userData = {
                "id": user._id,
                "fullname": user.fullname,
                "birthday": dateOfBirth,
                "bio": user.bio,
                "telegramLogin":user.telegramLogin,
                "user":req.user,
                "admin": extra.checkAdmin(req)};
            res.setHeader('Content-Type', 'text/html');    
            res.render('changeInfo', userData);
    })
    .catch((err)=>res.status(404).render("404"));
}
else res.status(403).render("403")
});


router.post('/disable/:id',checkAdmin,function(req, res) {
    
   const id = req.params.id;

   user_c.disable(id)
             .then(()=>{
               res.redirect("/users/"+req.params.id);
             })
             .catch((err)=>res.status(403).render("403"))
});

router.get('/:id',function(req, res,next) {
    console.log(`got some request: ${req.params.id}`);
    let HisPage;
    let notHisPage;
    if(req.user._id.toString()!=req.params.id) {
        notHisPage = 1;
        if(req.user.role==1) HisPage = true;
    }
    else HisPage = true;
    user_c.getById(req.params.id)
        .then((choosed_user)=>{ 
            let dateOfBirth;
            if(choosed_user.dateOfBirth!=null){
                dateOfBirth = choosed_user.dateOfBirth.toLocaleDateString();
            }
            let isUser;
            let isAdmin;
            if(parseInt(choosed_user.role) == 0) isUser = "checked";
            else {isAdmin = "checked"}

            let Disable;
            let Undisable;
            if(choosed_user.isDisabled) Undisable = "Undisable";
            else Disable = "Disable";

            let telegramLogin = "Not defined";
            if(choosed_user.telegramLogin!=undefined && choosed_user.telegramLogin!=null){
                telegramLogin=choosed_user.telegramLogin;
            }
                const usersData = {
                    "bio":choosed_user.bio,
                    "fullname":choosed_user.fullname,
                    "login":choosed_user.login,
                    "_id":choosed_user._id,
                    "avaUrl":choosed_user.avaUrl,
                    "dateOfBirth": dateOfBirth,
                    "Disable": Disable,
                    "Undisable": Undisable,
                    "telegramLog":telegramLogin,
                    "user": req.user,
                    "admin": extra.checkAdmin(req),
                    "notHisPage": notHisPage,
                    "isUser": isUser,
                    "isAdmin": isAdmin,
                    "HisPage": HisPage,
                };    
                res.setHeader('Content-Type', 'text/html');
                res.render('user', usersData);
        })
        .catch((err)=>res.status(404).render("404"))  
});

router.post('/:id', function(req, res,next) {
    console.log(`got some request: ${req.params.id}`);
    if(req.user.role!=1 && req.user._id.toString()!=req.params.id){
        res.status(403).render("403");
        return;
    }
    
    user_c.getById(req.params.id)
        .then((choosed_user)=>{
            choosed_user.bio = req.body.bio;
            choosed_user.fullname = req.body.fullname;
            choosed_user.dateOfBirth = req.body.bday;
            choosed_user.telegramLogin = req.body.telegramLogin;
            return user_c.update(choosed_user);
        })
        .then((id) => res.redirect("/users/"+id.toString()))
        .catch((err)=>res.status(404).render("404"))  
});

router.post('/change_role/:id', function(req, res,next) {

    user_c.getById(req.params.id)
        .then((choosed_user)=>{ 
                choosed_user.role = parseInt(req.body.role);
                return user_c.update(choosed_user);      
        })
        .then((id)=>res.redirect("/users/"+id))
        .catch((err)=>res.status(404).render("404"))  
});

module.exports = router;