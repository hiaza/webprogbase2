const express = require('express');
const user_c = require('../models/user');
const test_c = require('../models/test');
const question_c = require('../models/question');
const path = require('path');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');


const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(busboyBodyParser({ limit: '5mb' }));

const extra = require('../models/extra');

router.get('/',function(req, res,next) {
    
        /*test_c.getAll()
            .then((data)=>extra.pagination(res,req,'tests',data,5))
            .catch((err)=> res.status(404).send(err.mes))*/
        res.setHeader('Content-Type', 'text/html');
        res.render("testPassing.mst",{ 
            "user":         req.user,
            "admin":        extra.checkAdmin(req)
        });

});


router.get('/:id',function(req, res) {
    
    test_c.getById(req.params.id)
        .then((choosed_test)=>{
            return Promise.all([choosed_test,user_c.getById(choosed_test.authorID)])})
        .then(([choosed_test,author])=>{
            return Promise.all([choosed_test,author,question_c.getAll()])})
        .then(([choosed_test,author,pack])=>{
                let ret = [];
                for(let question of pack){
                    let i = 0;
                    while(i<question.tests.length){
                        if(question.tests[i].toString()==choosed_test.id.toString()) {
                            ret.push(question);
                            break;
                        }
                        i++;
                    }
                }
                const testData = {
        
                    "id":                   choosed_test.id,
                    "name":                 choosed_test.name,
                    "picUrl":               choosed_test.picUrl,
                    "author":               author.login,
                    "authorUrl":            "../users/"+author.id,
                    "created":              choosed_test.created,
                    "rating":               choosed_test.rating,
                    "subject":              choosed_test.subject,
                    "numOfpeopleWhoPassed": choosed_test.numOfpeopleWhoPassed,
                    "numOfQuestions":       choosed_test.numOfQuestions,
                    "all_items":            ret,
                    "userId":               req.user._id,
                    "user":                 req.user,
                    "admin":                extra.checkAdmin(req)             
                };    
                res.setHeader('Content-Type', 'text/html');
                res.render('testPassing', testData);
            }) 
    .catch((err)=>res.sendStatus(404))
});
  
module.exports = router;