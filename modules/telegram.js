const config = require('../config');

const TelegramBotApi = require('telegram-bot-api');

const user_c = require('../models/user');
const question_c = require('../models/question');
const test_c = require('../models/test');

const telegramBotApi = new TelegramBotApi({
    token: config.bot_token,
    updates: {
        enabled: true // do message pull
    }
});

telegramBotApi.on('message', onMessage);


function onMessage(message) {
    processRequest(message)
        .catch(err => telegramBotApi.sendMessage({
            chat_id: message.chat.id,
            text: `Something went wrong. Try again later. Error: ${err.toString()}`,
        }));
}

async function processRequest(message) {
    const chatId = message.chat.id;

    if (message.text === '/start' || message.text === "/config") {
            let user = await user_c.getAndRegisterByTName(message.from.username,chatId);
            if(user!=null){
                console.log(`User ${chatId} registered`);
                return telegramBotApi.sendMessage({
                    chat_id: chatId,
                    text: "Welcome to mango " + user.fullname,
                });    
            }else{
                return telegramBotApi.sendMessage({
                    chat_id: chatId,
                    text: "We don't know who you are :( Please change your telegram name on mango-tests user page",
                });
            }
        
    }else if (message.text === '/checkConnection') {
        let status = await user_c.isConnectedWithTelegram(message.from.username,chatId);
        let mes;
        if(status.connected){
            mes =  "All right! Your account is connected with telegram"
        }else if (status.found){
            mes = "Use /config to set up telegram";
        } else mes = "Change your telegram login on the user page, and then use /config for setting up telegram";

        return telegramBotApi.sendMessage({
            chat_id: chatId,
            text: mes,
            parse_mode: 'Markdown'
        });

    }else if(message.text === "/users"){
        let user = await user_c.getByTelegramName(message.from.username);
        let mes ="";
        if(user!=null){
            if(user.role==1){
                let allUsers = await user_c.getAll();
                for(let tempUser of allUsers){
                    if(tempUser.login!= user.login) 
                    mes += `[${tempUser.login}](https://mango-tests.herokuapp.com/users/${tempUser._id})\n`;
                }
                if(mes=="") mes = "No users registered yet"
            }
            else mes = "forbidden! Only for admins";
        }else{
            mes = "We don't know who you are :( Please change your telegram name on mango-tests user page";
        }
        return telegramBotApi.sendMessage({
            chat_id: chatId,
            text: mes,
            parse_mode: 'Markdown'
        });
    }else if(message.text === "/questions"){
        let user = await user_c.getByTelegramName(message.from.username);
        let mes ="";
        if(user!=null){
            let allQuestions = await question_c.getAll();
            
            for(let tempQuestion of allQuestions){
                if(tempQuestion.authorId == user._id) 
                    mes += `[${tempQuestion.question}](https://mango-tests.herokuapp.com/questions/${tempQuestion._id})\n`;    
            }
            if(mes==""){
                mes = "You haven't created any question"
            }
                
        }else{
            mes = "We don't know who you are :( Please change your telegram name on mango-tests user page";
        }
        return telegramBotApi.sendMessage({
            chat_id: chatId,
            text: mes,
            parse_mode: 'Markdown'
        });
    }else if(message.text === "/tests"){
        let user = await user_c.getByTelegramName(message.from.username);
        let mes ="";
        if(user!=null){
            let allTests = await test_c.getAll();
            for(let tempTest of allTests){ 
                mes += `[${tempTest.name}](https://mango-tests.herokuapp.com/test/${tempTest._id})\n`;    
            }
            if(mes==""){
                mes = "Nobody has created any test"
            }
        }else{
            mes = "We don't know who you are :( Please change your telegram name on mango-tests user page";
        }
        return telegramBotApi.sendMessage({
            chat_id: chatId,
            text: mes,
            parse_mode: 'Markdown'
        });
    }else return telegramBotApi.sendMessage({
        chat_id: chatId,
        text: "Can't recognise commant. List of commands:\n/start\n/config\n/checkConnection\n/users\n/tests\n/questions",
    });

    console.log(message.from.username, message.text);
}

module.exports = {
    async sendNotification(text,userID) {
        const user = await user_c.getById(userID);

        if(user.chatId!==undefined && user.chatId!=null){
            await telegramBotApi.sendMessage({
                chat_id: user.chatId,
                text: text,
                parse_mode: 'Markdown'
            });
        }
    }
};

